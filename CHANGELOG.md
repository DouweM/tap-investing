# Changelog3

## v0.0.2

- [#3](https://gitlab.com/DouweM/tap-investing/-/issues/3) Fix JSON error by bumping investpy to v1.0.6

## v0.0.1

Initial release
