"""Investing tap class."""

from typing import List
from singer_sdk import Tap, Stream
from singer_sdk.typing import (
    ArrayType,
    DateTimeType,
    NumberType,
    ObjectType,
    PropertiesList,
    Property,
    StringType,
)

from tap_investing.streams import (
    AssetsStream,
)

PLUGIN_NAME = "tap-investing"

STREAM_TYPES = [
    AssetsStream,
]


class TapInvesting(Tap):
    """Investing tap class."""

    name = "tap-investing"
    config_jsonschema = PropertiesList(
        Property(
            "assets",
            ArrayType(
                ObjectType(
                    Property("identifier", StringType, required=True),
                    Property("name", StringType),
                    Property("amount", NumberType),  # TODO: Set default
                )
            ),
            required=True,
        ),
        Property("start_date", DateTimeType),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


# CLI Execution:

cli = TapInvesting.cli
