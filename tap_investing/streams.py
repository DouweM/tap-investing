"""Stream class for tap-investing."""

import decimal
from tessa import Symbol

from copy import deepcopy
from typing import Iterable, List

from singer_sdk import Tap, Stream
from singer_sdk.typing import (
    NumberType,
    PropertiesList,
    Property,
    StringType,
)


class TapInvestingStream(Stream):
    pass


class AssetsStream(TapInvestingStream):
    name = "assets"

    schema = PropertiesList(
        Property("identifier", StringType, required=True),
        Property("name", StringType),
        Property("price", NumberType),
        Property("currency", StringType),
    ).to_dict()
    primary_keys = ["identifier"]

    def __init__(self, tap: Tap):
        super().__init__(tap=tap, name=None, schema=None)

    @property
    def partitions(self) -> List[dict]:
        return self.config.get("assets", [])

    def get_records(self, partition: dict) -> Iterable[dict]:
        identifier = partition["identifier"]
        name = partition.get("name")
        amount = partition.get("amount", 1)

        price_data = Symbol(identifier).price_latest()

        decimal.getcontext().prec = 10
        price = decimal.Decimal(str(price_data.price)) * decimal.Decimal(amount)

        record = {}
        record["identifier"] = identifier
        if name:
            record["name"] = name
        record["price"] = price
        record["currency"] = price_data.currency

        yield record
