# tap-investing

`tap-investing` is a Singer tap for [Yahoo Finance](https://finance.yahoo.com/)
that can be used to pull the prices of assets of various types.

Powered by [`tessa`](https://github.com/ymyke/tessa).
Built with [Meltano Singer SDK](https://sdk.meltano.com/).

## Usage with Meltano

You can easily run `tap-investing` by itself or in a pipeline using [Meltano](https://www.meltano.com/).

### Installation

1. [Install Meltano](http://meltano.com/docs/getting-started.html#install-meltano):

    ```bash
    pip install meltano
    ```

3. [Create a project](http://meltano.com/docs/getting-started.html#create-your-meltano-project):

    ```bash
    meltano init tap-investing-playground
    cd tap-investing-playground
    ```

2. Add `tap-investing` to `meltano.yml` under `plugins`/`extractors` as a custom plugin:

    ```yaml
    plugins:
      extractors:
      - name: tap-investing
        namespace: tap_investing
        pip_url: git+https://gitlab.com/DouweM/tap-investing.git
        settings:
        - name: assets
          kind: array
    ```

3. Install `tap-investing`:

    ```bash
    meltano install extractor tap-investing
    ```

### Configuration

Add `assets` to the `tap-investing` definition in `meltano.yml` under `config`:

```yaml
plugins:
  extractors:
  - name: tap-investing
    # ...
    config:
      assets:
      - name: Apple
        identifier: AAPL
      - name: S&P 500
        identifier: US500
      - name: Northern Trust Wld
        identifier: NL0011225305
        amount: 80
      - name: Northern Trust Em
        identifier: NL0011515424
        amount: 20
```

### Usage

#### Extract only

Run `tap-investing` by itself using `meltano invoke`:

```bash
meltano invoke tap-investing
```

#### Extract & Load

Run `tap-investing` in a pipeline with a loader using `meltano elt`:

```bash
# Add target-jsonl loader
meltano add loader target-jsonl

# Create output directory
mkdir output

# Run pipeline
meltano elt tap-investing target-jsonl

# View results
cat output/assets.jsonl
```

You should see output along the following lines:

```json
{"identifier": "AAPL", "name": "Apple", "price": 121.42}
{"identifier": "US500", "name": "S&P 500", "price": 3841.94}
{"identifier": "NL0011225305", "name": "Northern Trust Wld", "price": 1310.0}
{"identifier": "NL0011515424", "name": "Northern Trust Em", "price": 327.5}
```

## Stand-alone usage

### Installation

```bash
pip install git+https://gitlab.com/DouweM/tap-investing.git
```

### Configuration

Create a `config.json` file with content along the following lines:

```json
{
  "assets": [
    {
      "name": "Apple",
      "identifier": "AAPL"
    },
    {
      "name": "Northern Trust Wld",
      "identifier": "NL0011225305",
      "amount": 80
    },
    {
      "name": "Northern Trust Em",
      "identifier": "NL0011515424",
      "amount": 20
    }
  ]
}

```

### Usage

Run `tap-investing` with the `config.json` file created above:

```bash
tap-investing --config config.json
```
